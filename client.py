import socket

def main():
    host = 'localhost'
    port = 12345

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect((host, port))
        print(f"Connected to server: {host}:{port}")

        while True:
            data = s.recv(1024)
            num = int(data.decode())
            print(f"Received from server: {num}")

            if num >= 100:
                break  
            num += 1
            s.send(str(num).encode())

    except ConnectionRefusedError:
        print("Connection refused. Make sure the server is running.")
    finally:
        s.close()
        print("Connection closed with server.")

if __name__ == "__main__":
    main()
